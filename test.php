<?php
require 'TestHelper.php';

$values = [
    "Привет! Давно не виделись.",
    "When I wrote this code, only God and I understood what I did. Now only God knows",
    "У каждого языка  есть время жизни. За исключением 'Кобола', конечно",
    "Когда какой-то засранец скажет тебе: «У меня есть право на моё мнение», ты скажи: «А, да? А у меня есть право на моё мнение, а моё мнение в том, что у тебя нет прав на твоё мнение».",
];

foreach ($values as $value) {
    echo $value . PHP_EOL;
    echo TestHelper::revertCharacters($value) . PHP_EOL;
}
