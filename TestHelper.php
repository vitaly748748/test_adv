<?php

class TestHelper
{
    public static function revertCharacters(string $value): string
    {
        return preg_replace_callback('/[а-яёa-z]+/ui', static function($matches) {
            $word = $matches[0];
            $length = mb_strlen($word);
            $chars = [];
            $upCases = [];

            for ($index = 0; $index < $length; $index++) {
                $char = mb_substr($word, $index, 1);
                $upCases[] = mb_strtoupper($char) === $char;
                $chars[] = mb_strtolower($char);
            }

            $chars = array_reverse($chars);

            foreach ($upCases as $index => $upper) {
                if ($upper) {
                    $chars[$index] = mb_strtoupper($chars[$index]);
                }
            }

            return implode('', $chars);
        }, $value);
    }
}
